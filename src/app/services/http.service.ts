import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import * as constant from './constants';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { FormBuilder } from '@angular/forms';
import * as _ from 'lodash';


declare var $: any;


@Injectable()
export class HttpService {

    public filesData: any = [];
    modalRefArr: any = [];
    CONSTANT = constant;
    public readonly apiEndpoint: String;
    private loaderSubject = new BehaviorSubject<any>(null);
    public loaderStatus = this.loaderSubject.asObservable();

    private contactUpdatedSubject = new BehaviorSubject<any>(null);
    public contactUpdatedStatus = this.contactUpdatedSubject.asObservable();

    public eventSubject = new BehaviorSubject<any>(null);
    public eventStatus = this.eventSubject.asObservable();

    private modalSubject = new BehaviorSubject<any>(null);
    public modalStatus = this.modalSubject.asObservable();

    private searchSubject = new BehaviorSubject<any>(null);
    public searchStatus = this.searchSubject.asObservable();

    public heading: string;
    domain: string;
    loginData: any;
    myLoader = false;

    constructor(
        private router: Router, public http: HttpClient, public toastr: ToastrService,
        @Inject(DOCUMENT) public document: any, public fb: FormBuilder,
    ) {
        this.apiEndpoint = environment.apiBaseUrl;
    }




    navigate(url, params?) {
        if (params) {
            this.router.navigate([`/${url}`, params]);
        } else {
            this.router.navigate([`/${url}`]);
        }
    }



    postData(url, payload, isLoading?: boolean) {
        return this.http.post<any>(this.apiEndpoint + url, payload, { reportProgress: isLoading });
    }


    getCategoryDataParams(url, payload,id, isLoading?: boolean) {
         return this.http.post<any>(this.apiEndpoint + url + '/' + id , payload, { reportProgress: isLoading });
     }

    getData(url) {
        return this.http.get<any>(this.apiEndpoint + url);
    }

    getDataParams(url, payload, isLoading?: boolean) {
       var id = payload.videoId
        var form_data = new FormData();

        for (var key in payload) {
          form_data.append(key, payload[key]);
    
        }
        return this.http.post<any>(this.apiEndpoint + url + '/' + id , form_data, { reportProgress: isLoading });
    }

    getVideoList(url, obj?, isLoading?: boolean){

        let params = new HttpParams();
        if (obj) {
            Object.keys(obj).forEach(key => {
                if (obj[key] !== '' && obj[key] !== undefined) {
                    params = params.set(key, obj[key]);
                }
            });
        }
        return this.http.get<any>(this.apiEndpoint + url, { params: params, reportProgress: isLoading });

    }

    getVieoDetailsById(url,id) {
        return this.http.get<any>(this.apiEndpoint + url + '/' + id);
    }

    deleteById(url,id) {
        return this.http.delete(this.apiEndpoint + url + '/' + id);
    }

    approveVideo(url, payload,id, isLoading?: boolean) {
        // var id = payload.videoId
        //  var form_data = new FormData();
 
        //  for (var key in payload) {
        //    form_data.append(key, payload[key]);
     
        //  }
         return this.http.post<any>(this.apiEndpoint + url + '/approval/' + id , payload, { reportProgress: isLoading });
     }

     videoStatusUpdate(url, payload, isLoading?: boolean) {
        return this.http.post<any>(this.apiEndpoint + url+ '/' + payload, { reportProgress: isLoading });
    }

}


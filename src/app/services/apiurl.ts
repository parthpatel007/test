export const ApiUrl = {
    login: 'resend-otp',
    verifyotp:'verify-otp',
    resendotp:'resend-otp',
    videos:'videos',
    adminvideos:'user/videos',
    category:'categories',
    changevideoStatus:'videos/changeVideoStatus',
    user:'users',
    userdelete:'users/delete',
    userblock:'users/toggleStatus',
    categorystatuschange:'categories/changeStatus',
    dashboard:'dashboardData',
    languages:'languages',
    languageList:'languages',
    trainingVideo:'training-videos'
    
};


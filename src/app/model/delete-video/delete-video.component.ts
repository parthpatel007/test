import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { ApiUrl } from 'src/app/services/apiurl';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-delete-video',
  templateUrl: './delete-video.component.html',
  styleUrls: ['./delete-video.component.scss']
})
export class DeleteVideoComponent implements OnInit {

  videoId: any
  type: any
  description:any
  videoStatus:any


  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public http: HttpService,private toastr: ToastrService,public dialogRef: MatDialogRef<DeleteVideoComponent>) { }

  ngOnInit(): void {
    this.type = this.data.type
    this.videoId = this.data.id
    this.videoStatus = this.data.status
   
  
    this.dialogRef.backdropClick().subscribe(_ => {
      this.dialogRef.close('no');
    })
  }

  Video(type) {
    if (type == 'approve') {
      let payload = {
        isApproved:true
      }
        this.http.approveVideo(ApiUrl.videos,payload, this.videoId,false).subscribe((res) => {
          console.log(res)
          if (res.statusCode == 200) {

            this.toastr.success('Video approved successfully', 'success', {
              timeOut: 2000
            });
            this.dialogRef.close(this.dialogRef);

          }

      });
    }

    else if(type == 'reject'){

      if (this.toastr.currentlyActive) {
        return
      }
       
      if (this.description == undefined) {
        this.toastr.error('Description is required', 'Invalid', {
          timeOut: 3000
        });
        return;
  
      }
         
     let payload = {
      isApproved:false,
      reason:this.description
      }

      this.http.approveVideo(ApiUrl.videos,payload, this.videoId,false).subscribe((res) => {
        if (res.statusCode == 200) {
          this.toastr.success('Video rejected successfully', 'success', {
            timeOut: 2000
          });
          this.dialogRef.close(this.dialogRef);
        }

    });
    }
    else if(type == 'delete'){
      this.http.deleteById(ApiUrl.videos, this.videoId).subscribe((res) => {
      
          this.toastr.success('Video deleted successfully', 'success', {
            timeOut: 2000
          });
          this.dialogRef.close(this.dialogRef);
        

    });
    }
    else if(type == 'switch'){
      this.http.videoStatusUpdate(ApiUrl.changevideoStatus, this.videoId).subscribe((res) => {
        if (res.statusCode == 200) {
          if(this.videoStatus==true){
            this.toastr.success('Video active successfully', 'success', {
              timeOut: 2000
            });
          }else{
          this.toastr.success('Video inactive successfully', 'success', {
            timeOut: 2000
          });
        }
          this.dialogRef.close(this.dialogRef);
        }
    });
    }

    else if(type == 'block'){
    
      this.http.videoStatusUpdate(ApiUrl.changevideoStatus, this.videoId).subscribe((res) => {
        if (res.statusCode == 200) {
          this.toastr.success('Video block successfully', 'success', {
            timeOut: 2000
          });
          this.dialogRef.close(this.dialogRef);
        }
    });
    }

    else if(type == 'unblock'){
    
      this.http.videoStatusUpdate(ApiUrl.changevideoStatus, this.videoId).subscribe((res) => {
        if (res.statusCode == 200) {
          this.toastr.success('Video unblock successfully', 'success', {
            timeOut: 2000
          });
          this.dialogRef.close(this.dialogRef);
        }
    });
    }


    else if(type == 'blockUser'){
    
      this.http.videoStatusUpdate(ApiUrl.userblock, this.videoId).subscribe((res) => {
        if (res.statusCode == 200) {
          this.toastr.success('User block successfully', 'success', {
            timeOut: 2000
          });
          this.dialogRef.close(this.dialogRef);
        }
    });
    }

    else if(type == 'unblockUser'){
    
      this.http.videoStatusUpdate(ApiUrl.userblock, this.videoId).subscribe((res) => {
        if (res.statusCode == 200) {
          this.toastr.success('User unblock successfully', 'success', {
            timeOut: 2000
          });
          this.dialogRef.close(this.dialogRef);
        }
    });
    }

    else if(type == 'deleteLanguage'){
    
      this.http.deleteById(ApiUrl.languages, this.videoId).subscribe((res) => {
      
          this.toastr.success('Language deleted successfully', 'success', {
            timeOut: 2000
          });
          this.dialogRef.close(this.dialogRef);
        
    });
    }


    else if(type == 'logout'){
    
    localStorage.removeItem('accessToken');
    localStorage.removeItem('loginData');
    this.dialogRef.close(this.dialogRef);
    this.http.navigate('/login');

    }


    else if(type == 'deleteUser'){
      this.http.deleteById(ApiUrl.userdelete, this.videoId).subscribe((res) => {
      
          this.toastr.success('User deleted successfully', 'success', {
            timeOut: 2000
          });
          this.dialogRef.close(this.dialogRef);
        

    });
    }

    else if(type == 'deleteCategory'){
      this.http.deleteById(ApiUrl.category, this.videoId).subscribe((res) => {
          this.toastr.success('Category deleted successfully', 'success', {
            timeOut: 2000
          });
          this.dialogRef.close(this.dialogRef);
        

    });
    }

    else if(type == 'deletetraning'){
      this.http.deleteById(ApiUrl.trainingVideo, this.videoId).subscribe((res) => {
          this.toastr.success('training video deleted successfully', 'success', {
            timeOut: 2000
          });
          this.dialogRef.close(this.dialogRef);
        

    });
    }
    else if(type == 'blockCategory'){
      this.http.videoStatusUpdate(ApiUrl.categorystatuschange, this.videoId).subscribe((res) => {
          this.toastr.success('Category block successfully', 'success', {
            timeOut: 2000
          });
          this.dialogRef.close(this.dialogRef);
        

    });
    }

    else if(type == 'unblockCategory'){
      this.http.videoStatusUpdate(ApiUrl.categorystatuschange, this.videoId).subscribe((res) => {
          this.toastr.success('Category unblock successfully', 'success', {
            timeOut: 2000
          });
          this.dialogRef.close(this.dialogRef);
        

    });
    }





  }

  onKey(value){
  this.description=value
  }
}

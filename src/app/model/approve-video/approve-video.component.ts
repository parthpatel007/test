import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiUrl } from 'src/app/services/apiurl';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-approve-video',
  templateUrl: './approve-video.component.html',
  styleUrls: ['./approve-video.component.scss']
})
export class ApproveVideoComponent implements OnInit {

  videoData: any=[]
  thumbnailVideoImage:any
  imageUrl:any
  uploadvideoForm: FormGroup;
  videoFile: any
  imageFile: any
  checked: boolean = true;
  tagsArray=[]
  category=[]
  categoryPlaceholder:any='Category'
  categoryId:any
  button = 'Submit';
  isLoading = false;
  videoId:any
  categoryIdSelected:any
  public:boolean=true
  selected: boolean =true
  languageList=[]
  languagePlaceholder:any='Language'
  languageId:any
  duration:any
  videoUrl:any
  videoFileName:any
  videoParms=true
  trainingData:any = []

  constructor(private _formBuilder: FormBuilder,@Inject(MAT_DIALOG_DATA) public data: any, public http: HttpService,public dialogRef: MatDialogRef<ApproveVideoComponent>) { }

  ngOnInit(): void {
    this.dialogRef.backdropClick().subscribe(_ => {
      this.dialogRef.close('no');
    })
    this.videoData=this.data.approveData
    console.log(this.videoData)
    this.thumbnailVideoImage = this.data.thumbnailVideoImage
    this.imageUrl = this.data.imageUrl
    this.categoryPlaceholder = this.data.categoryPlaceholder
    this.languagePlaceholder = this.data.languagePlaceholder
    this.uploadvideos()
   this.traningData()
  }
  traningData(){
    this.uploadvideoForm.controls.title.setValue(this.videoData.title);
    this.uploadvideoForm.controls.description.setValue(this.videoData.description);
    const videoinputArr = this.f.videoinputRows as FormArray;
    this.videoData.videoinputRows.forEach(item => {
      console.log('item')
      console.log(item)
      videoinputArr.push(this._formBuilder.group({
        videoinput: [item.videoinput],
      }));
    });
     
     this.thumbnailVideoImage = this.thumbnailVideoImage
     this.imageUrl=this.imageUrl


   
   
  }





  

 

  uploadvideos() {

    this.uploadvideoForm = this._formBuilder.group({
      title: [''],
      description: [''],
      videoinputRows: this._formBuilder.array([])

    });

  }

  get videoinputForms() {
    return this.uploadvideoForm.get('videoinputRows') as FormArray;
  }


  save(){
    this.dialogRef.close(this.dialogRef);
  }
  
  getDuration(e) {
    this.duration = e.target.duration;
  }

  getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  

  cleardata(){
    this.dialogRef.close('no');
    // this.uploadvideoForm.reset();
    // let frmArray = this.uploadvideoForm.get('videoinputRows') as FormArray;
    // frmArray.clear();
    // this.categoryId=undefined
    // this.videoFile=undefined
    // this.imageFile=undefined
    // this.imageUrl=undefined
    // this.thumbnailVideoImage=undefined
    // this.categoryPlaceholder='Category'
   
  }


  deleteVideo() {
    this.thumbnailVideoImage = undefined;
    this.videoFile = undefined;
  }

  deleteImage() {
    this.imageUrl = undefined;
  }

  get f() { return this.uploadvideoForm.controls; }


  changeValue(value) {
    this.checked = !value;
  }

  

  


}
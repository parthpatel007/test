import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from 'src/app/services/authguard.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { TrainingVideoComponent } from './training-video.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

const routes: Routes = [
  {
      path: '', children: [
          {
              path: '',
              component:TrainingVideoComponent,
              canActivate: [AuthGuard],
              data: {title: 'training'},
          }
      ]
  }
];



@NgModule({
  declarations: [TrainingVideoComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    BsDropdownModule.forRoot(),
    ReactiveFormsModule,
    NgxSkeletonLoaderModule.forRoot()
  ]
})
export class TrainingVideoModule { }

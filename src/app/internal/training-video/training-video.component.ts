import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { DeleteVideoComponent } from 'src/app/model/delete-video/delete-video.component';
import { VideoPreviewComponent } from 'src/app/model/video-preview/video-preview.component';
import { ApiUrl } from 'src/app/services/apiurl';
import { HttpService } from 'src/app/services/http.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-training-video',
  templateUrl: './training-video.component.html',
  styleUrls: ['./training-video.component.scss']
})
export class TrainingVideoComponent implements OnInit {

  videoList = []
  category = []
  selectCategoryName: string = 'Categories'
  categoryId: any
  shimmer = true
  totalCount: any
  public pageNumber: number = 1;
  public pageSize: number = 10;
  public paginationValue: number = 0;
  searchChangeEventSubscription: Subscription;
  searchValue:any
  userId:any


  constructor(public http: HttpService,private _router: Router, public dialog: MatDialog, private toastr: ToastrService,public sharedserive: SharedService,public changedetecte: ChangeDetectorRef,private _route: ActivatedRoute) {
    this.sharedserive.searchDataChange.next(null);
   
   }

  ngOnInit(): void {
    this.getVideoList()
  }


  getVideoList() {
    this.shimmer = true
    let payload = {
      isGood:true
    }

    this.http.getVideoList(ApiUrl.trainingVideo,payload).subscribe((res) => {
      if (res.statusCode == 200) {
        this.shimmer = false
        this.videoList = res.data
      }

    });

  }

  
  previewVideo(data) {
    
console.log(data.video)

   
let video = data.video
  
    const dialogRef = this.dialog.open(VideoPreviewComponent, { panelClass: 'preview-video', data: { video } });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  deleteVideo(type,id) {

    const dialogRef = this.dialog.open(DeleteVideoComponent, { panelClass: 'delete-video', data: { type,id } });

    dialogRef.afterClosed().subscribe(result => {
      if(result!='no'){
        this.getVideoList()
        }
    });
  }


  uploadVideo(data){
    this._router.navigate(['/uploadvideo'], { queryParams: { videoUrl: JSON.stringify(data) } });  
  }


 
  trueValueFind(array){
    var count = array.filter(function(s) { return s.isGood; }).length;
    return count
  }


}


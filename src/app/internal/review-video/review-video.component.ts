import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { DeleteVideoComponent } from 'src/app/model/delete-video/delete-video.component';
import { VideoPreviewComponent } from 'src/app/model/video-preview/video-preview.component';
import { ApiUrl } from 'src/app/services/apiurl';
import { HttpService } from 'src/app/services/http.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-review-video',
  templateUrl: './review-video.component.html',
  styleUrls: ['./review-video.component.scss']
})
export class ReviewVideoComponent implements OnInit {

  videoList = []
  category = []
  selectCategoryName: string = 'Filter'
  categoryId: any
  shimmer = true
  totalCount: any
  public pageNumber: number = 1;
  public pageSize: number = 10;
  public paginationValue: number = 0;
  searchChangeEventSubscription: Subscription;
  searchValue:any
  tab : any 
  tab1 : any
  tab2 : any
  tab3 : any
  Clicked : boolean
  filterPlaceholder:any='Filter'
  videoStautus:any
  clearSearch: Subscription;

  constructor(public http: HttpService, public dialog: MatDialog, private toastr: ToastrService,public sharedserive: SharedService) {
    this.sharedserive.searchDataChange.next(null);

    this.searchChangeEventSubscription = this.sharedserive.searchDataChange.subscribe((data) => {
    
      if(data!=null){
        if(data.flag==true){
  
          this.shimmer = true
          this.videoList = [];
          this.paginationValue = 0;
          this.pageNumber = 1;
          this.pageSize = 10;
          this.searchValue=data.search
         this.getVideoList()
        }else if(data.flag==false) {
          
          this.shimmer = true
          this.videoList = [];
          this.paginationValue = 0;
          this.pageNumber = 1;
          this.pageSize = 10;
          this.searchValue=''
          this.getVideoList()
        }
      }
      })
   }

  ngOnInit(): void {
    // this.getCategory()
    this.getVideoList()
  }

  ngOnDestroy() {
    this.searchChangeEventSubscription.unsubscribe();
  }


  getCategory() {

    this.http.getData(ApiUrl.category).subscribe((res) => {
      if (res.data != undefined) {
        this.category = res.data
      }
    });

  }

  selectCategory(id, name) {
    this.shimmer = true
    this.videoList = [];
    this.paginationValue = 0;
    this.pageNumber = 1;
    this.pageSize = 10;
    this.selectCategoryName = name
    this.categoryId = id
    let payload={
      categoryId:this.categoryId,
      source: 'admin',
      skip: 0,
      limit: 10
    }
    this.http.getVideoList(ApiUrl.adminvideos, payload).subscribe((res) => {
      if (res.statusCode == 200) {
        this.shimmer = false
        this.videoList = res.data.videos
        this.totalCount = res.data.totalCount
      }
    });
  }

  selectAllCategories(){
    this.shimmer = true
    this.videoList = [];
    this.paginationValue = 0;
    this.pageNumber = 1;
    this.pageSize = 10;
    this.categoryId=''
    let payload={
      source: 'admin',
      skip: 0,
      limit: 10
    }
    this.http.getVideoList(ApiUrl.adminvideos, payload).subscribe((res) => {
      if (res.statusCode == 200) {
        this.shimmer = false
        this.videoList = res.data.videos
        this.totalCount = res.data.totalCount
      }
    });
  }

  



  getVideoList() {
    this.shimmer = true
    let payload = {
      source: 'admin',
      skip: 0,
      limit: 10,
      search:this.searchValue,
      isAdmin:true,
      status:this.videoStautus
    }

    this.http.getVideoList(ApiUrl.adminvideos, payload).subscribe((res) => {
      if (res.statusCode == 200) {
        this.shimmer = false
        this.videoList = res.data.videos
        this.totalCount = res.data.totalCount
      }

    });

  }

  categoryFilter() {

    if (this.toastr.currentlyActive) {
      return
    }

    if (this.categoryId == undefined) {
      this.toastr.error('Please select category', 'Invalid', {
        timeOut: 3000
      });
      return;

    }

    this.http.getDataParams(ApiUrl.videos, this.categoryId).subscribe((res) => {
      if (res.statusCode == 200) {
        this.videoList = res.data

      }

    });


  }

  previewVideo(video) {

    const dialogRef = this.dialog.open(VideoPreviewComponent, { panelClass: 'preview-video', data: { video } });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  deleteVideo(type,id) { 
      const dialogRef = this.dialog.open(DeleteVideoComponent, { panelClass: 'delete-video', data: { type,id } });
      dialogRef.afterClosed().subscribe(result => {
        if(result!='no'){
        this.getVideoList()
        }
       });
  }


  onScroll() {   
  if(this.videoList.length>0){
    if(this.videoList.length<this.totalCount){
    this.shimmer = true
      this.paginationValue = this.pageNumber * this.pageSize;
      this.pageNumber++;
      let payload = {
        source: 'admin',
        skip: this.paginationValue,
        limit: 10,
        categoryId:this.categoryId,
        isAdmin:true
      }
      this.http.getVideoList(ApiUrl.adminvideos, payload).subscribe((res) => {
        if (res.statusCode == 200) {
          this.shimmer = false
              res.data.videos.forEach(element => {
                this.videoList.push(element)
              });
           
        }

      });
    }
  }
  }


  filter(name){
    this.filterPlaceholder=name

    if(name=='Approved'){
      this.videoStautus = 'APPROVED'
      this.getVideoList()
    }else if(name=='Rejected'){
      this.videoStautus = 'REJECTED'
      this.getVideoList()
    }
    else if(name=='Filter'){
      this.videoStautus = ''
      this.getVideoList()
    }

  

  }



}

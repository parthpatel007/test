import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { DeleteVideoComponent } from 'src/app/model/delete-video/delete-video.component';
import { ReviewVideoComponent } from './review-video.component';
import { AuthGuardService as AuthGuard } from 'src/app/services/authguard.service';



const routes: Routes = [
  {
      path: '', children: [
          {
              path: '',
              component: ReviewVideoComponent,
              canActivate: [AuthGuard],
              data: {title: 'Reviewvideo'},
          }
      ]
  }
];


@NgModule({
  declarations: [
    ReviewVideoComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BsDropdownModule.forRoot(),
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    InfiniteScrollModule
    
  ], entryComponents: [
    DeleteVideoComponent
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReviewVideoModule { }

import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { DeleteVideoComponent } from 'src/app/model/delete-video/delete-video.component';
import { ApiUrl } from 'src/app/services/apiurl';
import { HttpService } from 'src/app/services/http.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})
export class RegisterUserComponent implements OnInit {

  userList:any=[];
  isLoading=false;
  shimmer=true
  searchValue:any
  searchChangeEventSubscription: Subscription;

  constructor(public http: HttpService,public dialog: MatDialog,public sharedserive: SharedService) { 
    this.sharedserive.searchDataChange.next(null);
    this.searchChangeEventSubscription = this.sharedserive.searchDataChange.subscribe((data) => {
     
      if(data!=null){
        if(data.flag==true){
          this.shimmer = true
          this.userList = []
          this.searchValue=data.search
          this.getUserList()
        }else if(data.flag==false){
          this.shimmer = true
          this.userList = []
          this.searchValue=''
          this.getUserList()
        }
      }
      })
   }

  ngOnInit(): void {

    this.getUserList()
  }

  ngOnDestroy() {
    this.searchChangeEventSubscription.unsubscribe();
  }



  getUserList() {
    let payload = {
      search:this.searchValue
    }

    this.http.getVideoList(ApiUrl.user,payload).subscribe((res) => {
      this.isLoading=false;
      if (res.statusCode == 200) {
        this.isLoading=true;
        this.shimmer=false
       this.userList=res.data
      
      }

    });

  }

  deleteVideo(type,id) {

    const dialogRef = this.dialog.open(DeleteVideoComponent, { panelClass: 'delete-video', data: { type,id } });

    dialogRef.afterClosed().subscribe(result => {
      if(result!='no'){
        this.shimmer = true
        this.getUserList()
        }
    });
  }

}

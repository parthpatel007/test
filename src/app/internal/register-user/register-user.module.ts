import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterUserComponent } from './register-user.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { DeleteVideoComponent } from 'src/app/model/delete-video/delete-video.component';
import { AuthGuardService as AuthGuard } from 'src/app/services/authguard.service';
import { AvatarModule } from "ngx-avatar";
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import {MatMenuModule} from '@angular/material/menu';



const routes: Routes = [
  {
      path: '', children: [
          {
              path: '',
              component: RegisterUserComponent,
              canActivate: [AuthGuard],
              data: {title: 'Registeruser'},
          }
      ]
  }
];


@NgModule({
  declarations: [
    RegisterUserComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    AvatarModule,
    NgxSkeletonLoaderModule.forRoot(),
    MatMenuModule
    
  ], entryComponents: [
    DeleteVideoComponent
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class RegisterUserModule { }

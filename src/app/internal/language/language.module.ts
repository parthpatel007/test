import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LanguageComponent } from './language.component';
import { AuthGuardService as AuthGuard } from 'src/app/services/authguard.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {
      path: '', children: [
          {
              path: '',
              component: LanguageComponent,
              canActivate: [AuthGuard],
              data: {title: 'language'},
          }
      ]
  }
];

@NgModule({
  declarations: [LanguageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    
  ]
})
export class LanguageModule { }

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiUrl } from 'src/app/services/apiurl';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.scss']
})
export class LanguageComponent implements OnInit {

  languageForm: FormGroup;
  button = 'Submit';
  submitted = false;
  isLoading = false;
  languageName:any;
  regional: any
  id:any;

  constructor(private _formBuilder: FormBuilder, private toastr: ToastrService, public http: HttpService, private _router: Router, private _route: ActivatedRoute) { }

  ngOnInit(): void {
    this.languageForms()
    this._route.queryParams.subscribe(params => {
      if (params['name'] != undefined) {
        this.languageForm.controls.name.setValue(params['name']);
        this.languageForm.controls.regional.setValue(params['regional']);
        this.languageName=params['name']
        this.regional=params['regional']
        this.id=params['id']
      }
    });


  }


  languageForms() {
    this.languageForm = this._formBuilder.group({
      name: [''],
      regional: [''],
    });
  }

  get f() { return this.languageForm.controls; }

  save() {

    this.submitted = true;

    if (this.toastr.currentlyActive) {
      return
    }

    if (this.languageForm.value.name == '' || this.languageForm.value.name == undefined) {
      this.toastr.error('Name is required', 'Invalid', {
        timeOut: 3000
      });
      return;
    }


    if (this.languageForm.value.regional == '' || this.languageForm.value.regional == undefined) {
      this.toastr.error('Regional is required', 'Invalid', {
        timeOut: 3000
      });
      return;
    }


    this.isLoading = true;
    this.button = 'PROCESSING';


    if (this.languageName != undefined) {

      let payload = {
        name: this.languageForm.value.name,
        regionalText:this.languageForm.value.regional
      }

      var form_data = new FormData();

      for (var key in payload) {
        form_data.append(key, payload[key]);

      }

      this.http.getCategoryDataParams(ApiUrl.languages, form_data, this.id, false)
        .subscribe(res => {

          if (res.statusCode == 200) {
            this.isLoading = false;
            this.button = 'Submit';
            this.toastr.success('Language updated successfully', 'success', {
              timeOut: 2000
            });
            this._router.navigate(['/languageList']);
          }

        },
          () => {

            this.isLoading = false;
            this.button = 'Submit';

          });

    }

    else {

      let payload = {
        name: this.languageForm.value.name,
        regionalText:this.languageForm.value.regional


      }
      var form_data = new FormData();

      for (var key in payload) {
        form_data.append(key, payload[key]);

      }

      this.http.postData(ApiUrl.languages, form_data, false)
        .subscribe(res => {

          if (res.statusCode == 200) {
            this.isLoading = false;
            this.button = 'Submit';
            this.toastr.success('language add successfully', 'success', {
              timeOut: 2000
            });
            this._router.navigate(['/languageList']);
          }

        },
          () => {

            this.isLoading = false;
            this.button = 'Submit';

          });



    }
  }

  cleardata() {
    this.languageForm.reset();

  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiUrl } from 'src/app/services/apiurl';
import { HttpService } from 'src/app/services/http.service';
import { VideoProcessingService } from 'src/app/services/video-processing-service';

@Component({
  selector: 'app-upload-category',
  templateUrl: './upload-category.component.html',
  styleUrls: ['./upload-category.component.scss']
})
export class UploadCategoryComponent implements OnInit {

  uploadcategoryForm: FormGroup;
  button = 'Submit';
  submitted = false;
  isLoading = false;
  public thumbnailVideoImage: any;
  imageUrl: any;
  videoFile: any
  imageFile: any
  imageCoverFile: any
  imageCoverUrl: any
  category_id: any;
  categoryDetails: any = []

  constructor(private _formBuilder: FormBuilder, private toastr: ToastrService, private videoService: VideoProcessingService, public http: HttpService, private _router: Router, private _route: ActivatedRoute) { }

  ngOnInit(): void {

    this._route.queryParams.subscribe(params => {
      if (params['id'] != undefined) {
        this.category_id = params['id'];
        this.categoryDetailsGet(this.category_id)
      }
    });

    this.categoryForm()

  }


  categoryDetailsGet(id) {

    let payload = {
      categoryId: id
    }

    this.http.getVideoList(ApiUrl.category, payload).subscribe((res) => {
      if (res.statusCode == 200) {
        this.categoryDetails = res.data
        this.uploadcategoryForm.controls.name.setValue(this.categoryDetails[0].name);
        this.imageUrl = this.categoryDetails[0].icon
        this.imageFile = this.categoryDetails[0].icon
        this.imageCoverUrl = this.categoryDetails[0].cover
        this.imageCoverFile = this.categoryDetails[0].cover

      }
    });
  }

  categoryForm() {
    this.uploadcategoryForm = this._formBuilder.group({
      name: [''],
    });
  }

  get f() { return this.uploadcategoryForm.controls; }

  save() {

    this.submitted = true;

    if (this.toastr.currentlyActive) {
      return
    }

    if (this.uploadcategoryForm.value.name == '' || this.uploadcategoryForm.value.name == undefined) {
      this.toastr.error('Name is required', 'Invalid', {
        timeOut: 3000
      });
      return;
    }

    if (this.imageFile == undefined) {
      this.toastr.error('Please upload icon', 'Invalid', {
        timeOut: 3000
      });
      return;
    }

    if (this.imageCoverFile == undefined) {
      this.toastr.error('Please upload cover image', 'Invalid', {
        timeOut: 3000
      });
      return;
    }


    this.isLoading = true;
    this.button = 'PROCESSING';


    if (this.category_id != undefined) {

      let payload = {
        name: this.uploadcategoryForm.value.name,
        iconFile: this.imageFile,
        coverFile: this.imageCoverFile
      }

      var form_data = new FormData();

      for (var key in payload) {
        form_data.append(key, payload[key]);

      }

      this.http.getCategoryDataParams(ApiUrl.category, form_data,this.category_id, false)
        .subscribe(res => {

          if (res.statusCode == 200) {
            this.isLoading = false;
            this.button = 'Submit';
            this.toastr.success('Category updated successfully', 'success', {
              timeOut: 2000
            });
            this._router.navigate(['/categories']);
          }

        },
          () => {

            this.isLoading = false;
            this.button = 'Submit';

          });

    }

    else {
      let payload = {
        name: this.uploadcategoryForm.value.name,
        iconFile: this.imageFile,
        coverFile: this.imageCoverFile
      }
      var form_data = new FormData();

      for (var key in payload) {
        form_data.append(key, payload[key]);

      }

      this.http.postData(ApiUrl.category, form_data, false)
        .subscribe(res => {

          if (res.statusCode == 200) {
            this.isLoading = false;
            this.button = 'Submit';
            this.toastr.success('Category add successfully', 'success', {
              timeOut: 2000
            });
            this._router.navigate(['/categories']);
          }

        },
          () => {

            this.isLoading = false;
            this.button = 'Submit';

          });


    }
  }
  deleteImage() {
    this.imageUrl = undefined;
  }

  deleteImages() {
    this.imageCoverUrl = undefined;
  }

  cleardata() {
    this.uploadcategoryForm.reset();

    this.imageFile = undefined
    this.imageUrl = undefined
    this.imageCoverFile = undefined
    this.imageCoverUrl = undefined

  }

  public imageVideo(): void {
    this.videoService.gifFile().then((image: File) => {
      this.imageFile = image
      if (image.size >= 5000000) {
        return
      }
      var reader = new FileReader();
      reader.readAsDataURL(image);
      reader.onload = (_event) => {
        this.imageUrl = reader.result

      }
    })
  }

  public imageVideos(): void {
    this.videoService.imageFile().then((image: File) => {
      this.imageCoverFile = image
      if (image.size >= 5000000) {
        return
      }
      var reader = new FileReader();
      reader.readAsDataURL(image);
      reader.onload = (_event) => {
        this.imageCoverUrl = reader.result

      }
    })
  }

}

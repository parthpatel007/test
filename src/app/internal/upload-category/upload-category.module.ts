import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { DeleteVideoComponent } from 'src/app/model/delete-video/delete-video.component';
import { AuthGuardService as AuthGuard } from 'src/app/services/authguard.service';
import { UploadCategoryComponent } from './upload-category.component';
import { VideoProcessingService } from 'src/app/services/video-processing-service';



const routes: Routes = [
  {
      path: '', children: [
          {
              path: '',
              component: UploadCategoryComponent,
              canActivate: [AuthGuard],
              data: {title: 'UploadCategory'},
          }
      ]
  }
];


@NgModule({
  declarations: [
    UploadCategoryComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BsDropdownModule.forRoot(),
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    InfiniteScrollModule
    
  ],
  providers: [
    VideoProcessingService
  ],
   entryComponents: [
    DeleteVideoComponent
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UploadCategoryModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './base/header/header.component';
import { SidebarComponent } from './base/sidebar/sidebar.component';
import { RouterModule, Routes } from '@angular/router';
import { InternalComponent } from './internal.component';














const routes: Routes = [{
  path: '', component: InternalComponent,
  children: [
      {
          path: 'dashboard',
          loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'uploadvideo',
        loadChildren: () => import('./upload-video/upload-video.module').then(m => m.UploadVideoModule)
      },
      {
        path: 'allvideos',
        loadChildren: () => import('./all-video/all-video.module').then(m => m.AllVideoModule)
      },
      {
        path: 'reviewvideos',
        loadChildren: () => import('./review-video/review-video.module').then(m => m.ReviewVideoModule)
      },
      {
        path: 'registeruser',
        loadChildren: () => import('./register-user/register-user.module').then(m => m.RegisterUserModule)
      },
      {
        path: 'categories',
        loadChildren: () => import('./categories/categories.module').then(m => m.CategoriesModule)
      },
      {
        path: 'uploadcategories',
        loadChildren: () => import('./upload-category/upload-category.module').then(m => m.UploadCategoryModule)
      },
      {
        path: 'language',
        loadChildren: () => import('./language/language.module').then(m => m.LanguageModule)
      },

      {
        path: 'languageList',
        loadChildren: () => import('./languages-list/languages-list.module').then(m => m.LanguagesListModule)
      },

      {
        path: 'trainingVideo',
        loadChildren: () => import('./training-video/training-video.module').then(m => m.TrainingVideoModule)
      },
      
    
  ]
}];





@NgModule({
  declarations: [
    InternalComponent,
    HeaderComponent,
    SidebarComponent
    
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class InternalModule { }

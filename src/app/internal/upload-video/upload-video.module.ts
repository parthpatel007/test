import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UploadVideoComponent } from './upload-video.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VideoProcessingService } from '../../services/video-processing-service';
import { AuthGuardService as AuthGuard } from 'src/app/services/authguard.service';
import { DeleteVideoComponent } from 'src/app/model/delete-video/delete-video.component';
import { ApproveVideoComponent } from 'src/app/model/approve-video/approve-video.component';


const routes: Routes = [
  {
      path: '', children: [
          {
            path: '',
            component: UploadVideoComponent,
            canActivate: [AuthGuard],
            data: {title: 'Dashboard'},
          }
      ]
  }
];




@NgModule({
  declarations: [UploadVideoComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BsDropdownModule.forRoot(),
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule
    
  ],
  providers: [
    VideoProcessingService
  ],
  entryComponents: [
    DeleteVideoComponent,
    ApproveVideoComponent
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UploadVideoModule { }

import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HttpService } from 'src/app/services/http.service';
import { VideoProcessingService } from '../../services/video-processing-service';
import { ApiUrl } from 'src/app/services/apiurl';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteVideoComponent } from 'src/app/model/delete-video/delete-video.component';
import { MatDialog } from '@angular/material/dialog';
import { ApproveVideoComponent } from 'src/app/model/approve-video/approve-video.component';



@Component({
  selector: 'app-upload-video',
  templateUrl: './upload-video.component.html',
  styleUrls: ['./upload-video.component.scss']
})
export class UploadVideoComponent implements OnInit {


  uploadvideoForm: FormGroup;
  public thumbnailVideoImage: any;
  imageUrl: any;
  videoFile: any
  imageFile: any
  checked: boolean = true;
  tagsArray=[]
  category=[]
  categoryPlaceholder:any='Category'
  categoryId:any
  button = 'Submit';
  isLoading = false;
  videoId:any
  videoDetails:any=[]
  categoryIdSelected:any
  public:boolean=true
  selected: boolean =true
  languageList=[]
  languagePlaceholder:any='Language'
  languageId:any
  duration:any
  videoUrl:any
  videoFileName:any
  videoParms=true
  trainingData:any = []
  score:any


  constructor(private _formBuilder: FormBuilder, private videoService: VideoProcessingService, private toastr: ToastrService, public http: HttpService,private _router: Router,
    private _route: ActivatedRoute,public dialog: MatDialog,private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
  
    this._route.queryParams.subscribe(params => {
      if(params['id']!=undefined){
      this.videoId = params['id'];
      this.videoDetailsGet(this.videoId)
      }
    });
   
      this.uploadvideos()
    this._route.queryParams.subscribe(params => {
      if(params['videoUrl']!=undefined){
        this.videoParms=false
         this.trainingData = JSON.parse(params['videoUrl'])
        this.getCategoryAndLanguage(this.trainingData)
        this.traningData(this.trainingData)
      
      }
    });
    if(this.trainingData.length==0){
      this.getCategory()
      this.getLanguage()
      }
    if(this.videoId==undefined && this.trainingData.length==0){
    this.addNewVideoRow()
    }
   
  }

  traningData(trainingData){
   
    this.uploadvideoForm.controls.title.setValue(trainingData.title);
    this.uploadvideoForm.controls.description.setValue(trainingData.description);
    const videoinputArr = this.f.videoinputRows as FormArray;
    this.trainingData.tags.forEach(item => {
      videoinputArr.push(this._formBuilder.group({
        videoinput: [item],
      }));
    });
     this.categoryId=this.trainingData.categoryId
     this.languageId=this.trainingData.languageId
     this.videoFile=this.trainingData.video
     this.thumbnailVideoImage = this.trainingData.video
     this.imageFile=this.trainingData.thumbnail
     this.imageUrl=this.trainingData.thumbnail
     this.videoFileName = this.trainingData.filename
     var count = this.trainingData.swappedVideos.filter(function(s) { return s.isGood; }).length;
     this.score = count+'/'+this.trainingData.swappedVideos.length

    

   
   
  }


 //videoDetailsGet
   videoDetailsGet(id){


    this.getLanguage()

    this.http.getVieoDetailsById(ApiUrl.videos,id).subscribe((res) => { 
      if(res.data!=undefined){
      this.videoDetails=res.data
      this.selected = this.videoDetails.isActive
      this.uploadvideoForm.controls.title.setValue(this.videoDetails.title.split("-")[0]);
      this.uploadvideoForm.controls.description.setValue(this.videoDetails.title.split("-")[1]);
      const videoinputArr = this.f.videoinputRows as FormArray;
      this.videoDetails.tags.forEach(item => {
        videoinputArr.push(this._formBuilder.group({
          videoinput: [item],
        }));
      });
      this.categoryId=this.videoDetails.categoryId._id
      this.categoryPlaceholder=this.videoDetails.categoryId.name
      this.languageId=this.videoDetails.languageId
      this.videoFile=this.videoDetails.video.original
      this.thumbnailVideoImage = this.videoDetails.video.original
      this.imageFile=this.videoDetails.video.thumbnail
      this.imageUrl=this.videoDetails.video.thumbnail
      this.videoFileName = this.videoDetails.video.fileName
      this.public=this.videoDetails.isPublic
      }
    });
   }

  
   

  getCategory(){

    this.http.getData(ApiUrl.category).subscribe((res) => { 
      if(res.data!=undefined){
      this.category=res.data
      }
    
    });

  }

  getLanguage(){
   
    this.http.getData(ApiUrl.languageList).subscribe((res) => { 
      if(res.data!=undefined){
      this.languageList=res.data
    //   console.log(this.languageList)
    //  console.log(this.videoDetails.languageId)
      
if(this.videoId!=undefined){
  let languageName = this.languageList.filter(x => x._id === this.videoDetails.languageId)
  console.log(languageName)
       this.languagePlaceholder=languageName[0].regionalText
       this.ref.detectChanges();
}
      }
    
    });

  }

  getCategoryAndLanguage(trainingData){

    console.log(trainingData)
let categoryId = trainingData.categoryId
let language = trainingData.languageId
this.http.getData(ApiUrl.category).subscribe((res) => { 
  if(res.data!=undefined){
  this.category=res.data
  
  let categoryName = this.category.filter(x => x._id === categoryId)
  if(categoryName.length>0){
  this.categoryPlaceholder=categoryName[0].name

  
  }
  }

});


this.http.getData(ApiUrl.languageList).subscribe((res) => { 
  if(res.data!=undefined){
  this.languageList=res.data
  let languageNames = this.languageList.filter(x => x._id === language)
  if(languageNames.length>0){
  this.languagePlaceholder=languageNames[0].regionalText
}
}
  

});
  }
  

  selectCategory(id,name){
    this.categoryPlaceholder=name
    this.categoryId=id

  }

  selectLanguage(id,name){
    this.languagePlaceholder=name
    this.languageId=id

  }

  uploadvideos() {

    this.uploadvideoForm = this._formBuilder.group({
      title: [''],
      description: [''],
      videoinputRows: this._formBuilder.array([])

    });

  }

  get videoinputForms() {
    return this.uploadvideoForm.get('videoinputRows') as FormArray;
  }


  addNewVideoRow() {
    let self = this;
    const videoinputArr = self.f.videoinputRows as FormArray;
    if (videoinputArr.value.length == 5) {
      return
    }
    videoinputArr.push(this._formBuilder.group({
      videoinput: [''],
    }));


  }

  deleteVideoRow(index: number) {
    let videoinputArr = this.f.videoinputRows as FormArray;
    videoinputArr.removeAt(index);
  }

  public uploadVideo(): void {
    this.videoService.promptForVideo().then(videoFile => {
      this.videoFileName = videoFile.name
      if(videoFile.size>=10000000){
        return
       }
      this.videoFile = videoFile
      var reader = new FileReader();
      reader.readAsDataURL(videoFile);
      reader.onload = (_event) => {
        this.thumbnailVideoImage = reader.result
  
      }
      return this.videoService.generateThumbnail(videoFile);
    }).then(thumbnailData => {
      //this.thumbnailVideoImage = thumbnailData;

    })
  }

  getDuration(e) {
    this.duration = e.target.duration;
  }

  getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  public imageVideo(): void {
    this.videoService.imageFile().then((image: File) => {
      console.log(image);
      this.imageFile = image
      if (image.size >= 5000000) {
        return
      }
      var reader = new FileReader();
      reader.readAsDataURL(image);
      reader.onload = (_event) => {
        this.imageUrl = reader.result
      }
    })
  }




  deleteVideo() {
    this.thumbnailVideoImage = undefined;
    this.videoFile = undefined;
  }

  deleteImage() {
    this.imageUrl = undefined;
  }

  get f() { return this.uploadvideoForm.controls; }


  changeValue(value) {
    this.checked = !value;
  }

  cleardata(){
    this.uploadvideoForm.reset();
    let frmArray = this.uploadvideoForm.get('videoinputRows') as FormArray;
    frmArray.clear();
    this.categoryId=undefined
    this.videoFile=undefined
    this.imageFile=undefined
    this.imageUrl=undefined
    this.thumbnailVideoImage=undefined
    this.categoryPlaceholder='Category'
    this.addNewVideoRow()
  }


  save() {

    

  
    if (this.toastr.currentlyActive) {
      return
    }

    if (this.uploadvideoForm.value.title == '' || this.uploadvideoForm.value.title == undefined) {
      this.toastr.error('Title is required', 'Invalid', {
        timeOut: 3000
      });
      return;

    }

    else if (this.uploadvideoForm.value.description == '' || this.uploadvideoForm.value.description == undefined) {
      this.toastr.error('Description is required', 'Invalid', {
        timeOut: 3000
      });
      return;

    }

    else if (this.uploadvideoForm.value.videoinputRows.length == 0) {
      this.toastr.error('Tags is required', 'Invalid', {
        timeOut: 3000
      });
     
      return;

    }


    else if (this.categoryId == undefined || this.categoryPlaceholder=='Category') {
      this.toastr.error('Please select category', 'Invalid', {
        timeOut: 3000
      });
      return;

    }
    else if (this.languageId == undefined || this.languagePlaceholder=='Language') {
      this.toastr.error('Please select language', 'Invalid', {
        timeOut: 3000
      });
      return;

    }



    else if (this.videoFile == undefined) {
      if(this.trainingData.length==0){
      this.toastr.error('Please upload video', 'Invalid', {
        timeOut: 3000
      });
      return;
    }
    }


    else if (this.imageUrl == undefined) {
      this.toastr.error('Please upload cover image', 'Invalid', {
        timeOut: 3000
      });
      return;

    }


    // this.isLoading = true;
    // this.button = 'Processing';


    this.tagsArray=[]
    this.uploadvideoForm.value.videoinputRows.forEach(element => {
      this.tagsArray.push(element.videoinput)
    });


    if(this.videoId!=undefined){

      let payload = {
        videoId:this.videoId,
        title: this.uploadvideoForm.value.title+'-'+this.uploadvideoForm.value.description,
        //description:this.uploadvideoForm.value.description,
        tags:JSON.stringify(this.tagsArray),
        videoFile: this.videoFile,
        thumbnailFile: this.imageFile,
        categoryId:this.categoryId,
        isPublic: this.checked,
        languageId :this.languageId,
        duration:this.duration,
        fileName:this.videoFileName
  
      }
      

  

      this.http.getDataParams(ApiUrl.videos, payload, false)
      .subscribe(res => {
      
        if(res.statusCode==200){
          this.isLoading = false;
          this.button = 'Submit';
          this.toastr.success('Video updated successfully', 'success', {
            timeOut: 2000
          });
          this._router.navigate(['/allvideos']);
        }
    
      },
        () => {

          this.isLoading = false;
          this.button = 'Submit';

        });
      
    }else{


let approveData=this.uploadvideoForm.value
let thumbnailVideoImage = this.thumbnailVideoImage
let imageUrl = this.imageUrl
let categoryPlaceholder = this.categoryPlaceholder
let languagePlaceholder = this.languagePlaceholder
    const dialogRef = this.dialog.open(ApproveVideoComponent, { panelClass: 'approve-video', data: { approveData,thumbnailVideoImage,imageUrl,categoryPlaceholder,languagePlaceholder } });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if(result!='no'){
           let payload = {
        title: this.uploadvideoForm.value.title+'-'+this.uploadvideoForm.value.description,
        //description:this.uploadvideoForm.value.description,
        tags:JSON.stringify(this.tagsArray),
        videoFile: this.videoFile,
        thumbnailFile: this.imageFile,
        categoryId:this.categoryId,
        isPublic: this.checked,
        languageId :this.languageId,
        duration:this.duration,
        fileName:this.videoFileName,
        score:this.score
        // videoThumbnail:this.thumbnailVideoImage
  
      }

      console.log(payload)

      var form_data = new FormData();

      for (var key in payload) {
        form_data.append(key, payload[key]);
  
      }

      this.http.postData(ApiUrl.videos, form_data, false)
      .subscribe(res => {
      
        if(res.statusCode==200){
          this.isLoading = false;
          this.button = 'Submit';
          this.toastr.success('Video uploaded successfully', 'success', {
            timeOut: 2000
          });
          this._router.navigate(['/allvideos']);
        }
    
      },
        () => {

          this.isLoading = false;
          this.button = 'Submit';

        });
        }
    });

   
    }
  
  }

  onValChange(value){
   let status = this.selected
  let type = 'switch'
  let id  = this.videoId 
    const dialogRef = this.dialog.open(DeleteVideoComponent, { panelClass: 'delete-video', data: { type,id,status } });
      dialogRef.afterClosed().subscribe(result => {
    
        if(result!='no'){
          let frmArray = this.uploadvideoForm.get('videoinputRows') as FormArray;
          frmArray.clear();
        this.videoDetailsGet(this.videoId)
          }else{
            this.selected = this.videoDetails.isActive
          }
         });
  
}
}



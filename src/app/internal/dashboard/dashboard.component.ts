import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { BaseChartDirective, Color, Label } from 'ng2-charts';
import { ApiUrl } from 'src/app/services/apiurl';
import { HttpService } from 'src/app/services/http.service';
import { DatePipe } from '@angular/common';
import { DaterangepickerDirective } from 'ngx-daterangepicker-material';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  @ViewChild(DaterangepickerDirective, {static: true}) picker: DaterangepickerDirective;
  selected: {startDate: moment.Moment, endDate: moment.Moment};

  @ViewChild(BaseChartDirective) chart: BaseChartDirective;
  bsValue: Date = new Date();
  dashboard:any=[]
  registerUser:any=[]
  registerUserDay:any=[]
  startDates:any;
  endDates:any;







  //first box chart

  public lineChartData: ChartDataSets[] = [
    { data: [],fill:false },
  ];

  public lineChartLabels: Label[] = [];

  public lineChartOptions: ChartOptions  = {
    responsive: true,

     scales: {
        xAxes: [{
            gridLines: {
                color: "rgba(0, 0, 0, 0)",
                zeroLineColor: '#F0F0F0'
            },
            ticks: {
              fontColor: "#FFFFFF", // this here
            },
        }],
        yAxes: [{
            gridLines: {
                color: "rgba(0, 0, 0, 0)",
                zeroLineColor: '#F0F0F0'
            },
            ticks: {
              fontColor: "#FFFFFF", // this here
            },
        }]
    },

  };
  

  public lineChartColors: Color[] = [
    {
      borderColor: '#A6CEE3',
  
    },
  ];
  public lineChartLegend = false;
  public lineChartType = 'line';
  public lineChartPlugins = [];


  //second box



  public lineChartData2: ChartDataSets[] = [
    { data: [65, 59, 80, 100, 56, 55, 0]},
  ];

  public lineChartLabels2: Label[] = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

  public lineChartOptions2: ChartOptions  = {
    responsive: true,

     scales: {
        xAxes: [{
            gridLines: {
                color: "rgba(0, 0, 0, 0)",
                zeroLineColor: '#F0F0F0'
            },
            ticks: {
              fontColor: "#FFFFFF", // this here
            },
        }],
        yAxes: [{
            gridLines: {
                color: "rgba(0, 0, 0, 0)",
                zeroLineColor: '#F0F0F0'
            },
            ticks: {
              fontColor: "#FFFFFF", // this here
            },  
        }]
    },

  };
  

  public lineChartColors2: Color[] = [
    {
      backgroundColor: 'rgba(251, 154, 153, 0.8)',
  
    },
  ];
  public lineChartLegend2 = false;
  public lineChartType2 = 'line';
  public lineChartPlugins2 = [];

  //box 3 chart


  public lineChartData3: ChartDataSets[] = [
    {barThickness: 16, data: [65,85,59, 80, 100, 56, 55, 0]},
  ];

  public lineChartLabels3: Label[] = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

  public lineChartOptions3: ChartOptions  = {
    responsive: true,
    
     scales: {
        xAxes: [{
            gridLines: {
                color: "rgba(0, 0, 0, 0)",
                zeroLineColor: '#F0F0F0',
                
                
                
            },
            ticks: {
              fontColor: "#FFFFFF", // this here
            },
        }],
        yAxes: [{
            gridLines: {
                color: "rgba(0, 0, 0, 0)",
                zeroLineColor: '#F0F0F0'
                
            },
            ticks: {
              fontColor: "#FFFFFF", // this here
            },
        }]
    },

  };
  

  public lineChartColors3: Color[] = [
    {
      backgroundColor: '#B2DF8A',
      
  
    },
  ];
  public lineChartLegend3 = false;
  public lineChartType3 = 'bar';
  public lineChartPlugins3 = [];


//fourth box



public lineChartData4: ChartDataSets[] = [
  { barThickness: 16, data: [90,75,51,65,85,59, 80, 100, 56, 55, 0]},
];



public lineChartLabels4: Label[] = ['Happy', 'Sad', 'Busy', 'Attitude', 'Music', 'Fun', 'Motivation', 'Party', 'Love', 'Angry'];

public lineChartOptions4: ChartOptions  = {
  responsive: true,
  
   scales: {
      xAxes: [{
          gridLines: {
              color: "rgba(0, 0, 0, 0)",
              zeroLineColor: '#F0F0F0',
              
            
          },
          ticks: {
            fontColor: "#FFFFFF", // this here
          },
      }],
      yAxes: [{
          gridLines: {
              color: "rgba(0, 0, 0, 0)",
              zeroLineColor: '#F0F0F0',
              
          },
          ticks: {
            fontColor: "#FFFFFF", // this here
          },
      }]
  },

  

};


public lineChartColors4: Color[] = [
  {
    backgroundColor: '#1F78B4',
    

  },
];
public lineChartLegend4 = false;
public lineChartType4 = 'bar';
public lineChartPlugins4 = [];


  constructor(public http: HttpService,private datePipe: DatePipe,public changedetecte: ChangeDetectorRef) { }

  myDateValue: Date;

  ngOnInit() {

    var d = new Date();
    this.endDates=this.datePipe.transform(d.toLocaleString(),"yyyy-MM-dd");
    d.setDate(d.getDate() - 7);
    this.startDates=this.datePipe.transform(d.toLocaleString(),"yyyy-MM-dd");

    // this.getDashboardData();
    this.myDateValue = new Date();

  }



  choosedDate(date){

    if(date.start!=null && date.end!=null){
    this.startDates=this.datePipe.transform(date.start._d.toLocaleString(),"yyyy-MM-dd");
    this.endDates=this.datePipe.transform(date.end._d.toLocaleString(),"yyyy-MM-dd");
    this.getDashboardData()
   
     } 
    }





  getDashboardData(){
    
    let payload = {
      'startDate':this.startDates,
       'endDate':this.endDates
     }

    this.http.getVideoList(ApiUrl.dashboard,payload).subscribe((res) => {
    
      if (res.statusCode == 200) {
        this.dashboard = res.data
        if(res.data.totalUser.length>0){
          this.registerUser=[]
          this.lineChartLabels=[]
       res.data.totalUser.forEach(element => {
         this.registerUser.push(element.totalVideo)
         this.lineChartData.push(
          { data: this.registerUser, fill: false }
          );
          
           this.lineChartColors = [
            {
              borderColor: '#A6CEE3',
          
            },
          ];
          
       
         this.lineChartLabels.push(element._id.day+'/'+element._id.month)
         this.changedetecte.detectChanges();
       });
        }
      }
    });
  }

  onDateChange(newDate: Date) {
    console.log(newDate);
  }

  

}

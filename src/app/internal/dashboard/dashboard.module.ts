import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { AuthGuardService as AuthGuard } from 'src/app/services/authguard.service';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ChartsModule } from 'ng2-charts';
import {DatePipe} from '@angular/common';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';




const routes: Routes = [
  {
      path: '', children: [
          {
            path: '',
            component: DashboardComponent,
            canActivate: [AuthGuard],
            data: {title: 'Dashboard'},
          }
      ]
  }
];



@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    ChartsModule,
    NgxDaterangepickerMd.forRoot()
   
    
    
  ],
  providers: [DatePipe],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardModule { }

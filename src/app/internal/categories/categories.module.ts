import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CategoriesComponent } from './categories.component';
import { AuthGuardService as AuthGuard } from 'src/app/services/authguard.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { DeleteVideoComponent } from 'src/app/model/delete-video/delete-video.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

const routes: Routes = [
  {
      path: '', children: [
          {
              path: '',
              component: CategoriesComponent,
              canActivate: [AuthGuard],
              data: {title: 'Categories'},
          }
      ]
  }
];

@NgModule({
  declarations: [CategoriesComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    NgxSkeletonLoaderModule.forRoot()
    
  ], entryComponents: [
    DeleteVideoComponent
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class CategoriesModule { }

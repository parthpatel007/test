import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AllVideoComponent } from './all-video.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuardService as AuthGuard } from 'src/app/services/authguard.service';
import { ReadMoreComponent } from './read-more/read-more.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { DeleteVideoComponent } from 'src/app/model/delete-video/delete-video.component';


const routes: Routes = [
  {
      path: '', children: [
          {
              path: '',
              component: AllVideoComponent,
              canActivate: [AuthGuard],
              data: {title: 'Allvideo'},
          }
      ]
  }
];



@NgModule({
  declarations: [
    AllVideoComponent,
    ReadMoreComponent
 
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BsDropdownModule.forRoot(),
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    InfiniteScrollModule
    
  ], entryComponents: [
    DeleteVideoComponent
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AllVideoModule { }

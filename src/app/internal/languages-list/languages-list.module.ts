import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from 'src/app/services/authguard.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LanguagesListComponent } from './languages-list.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

const routes: Routes = [
  {
      path: '', children: [
          {
              path: '',
              component:LanguagesListComponent,
              canActivate: [AuthGuard],
              data: {title: 'language'},
          }
      ]
  }
];



@NgModule({
  declarations: [LanguagesListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxSkeletonLoaderModule.forRoot()
  ]
})
export class LanguagesListModule { }

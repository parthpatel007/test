import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { DeleteVideoComponent } from 'src/app/model/delete-video/delete-video.component';
import { ApiUrl } from 'src/app/services/apiurl';
import { HttpService } from 'src/app/services/http.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-languages-list',
  templateUrl: './languages-list.component.html',
  styleUrls: ['./languages-list.component.scss']
})
export class LanguagesListComponent implements OnInit {

  category:any=[]
  shimmer=true
  searchValue:any
  searchChangeEventSubscription: Subscription;

  constructor(public http: HttpService,public dialog: MatDialog,public sharedserive: SharedService) { 
    this.sharedserive.searchDataChange.next(null);
    // this.searchChangeEventSubscription = this.sharedserive.searchDataChange.subscribe((data) => {
     
    //   if(data!=null){
    //     if(data.flag==true){
    //       this.shimmer = true
    //       this.category = []
    //       this.searchValue=data.search
    //       this.getCategory()
    //     }else if(data.flag==false){
    //       this.shimmer = true
    //       this.category = []
    //       this.searchValue=''
    //       this.getCategory()
    //     }
    //   }
    //   })
   }

  ngOnInit(): void {
    
    this.getCategory()
  }

  // ngOnDestroy() {
  //   this.searchChangeEventSubscription.unsubscribe();
  // }

  getCategory(){

    // let payload = {
    //   search:this.searchValue
    // }

    this.http.getData(ApiUrl.languageList).subscribe((res) => { 
      if(res.data!=undefined){
        this.shimmer=false
      this.category=res.data
      }
    
    });

  }

  deleteVideo(type,id) {

    const dialogRef = this.dialog.open(DeleteVideoComponent, { panelClass: 'delete-video', data: { type,id } });

    dialogRef.afterClosed().subscribe(result => {
      if(result!='no'){
        this.shimmer=true
        this.getCategory()
        }
    });
  }

}


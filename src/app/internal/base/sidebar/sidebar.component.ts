import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { SharedService } from 'src/app/services/shared.service';
declare var $:any

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(public http: HttpService,private sharedService: SharedService) { }
  ngOnInit(): void {

  
  }

  clearSearch(){
    this.sharedService.clearSearch();
  }

}

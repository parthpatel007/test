import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { DeleteVideoComponent } from 'src/app/model/delete-video/delete-video.component';
import { HttpService } from 'src/app/services/http.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @ViewChild('name') inputName;

  timeout: any = null;
  filterName: string;
  searchData: any
  openClose = false

  constructor(public http: HttpService, private sharedService: SharedService, private toastr: ToastrService, public dialog: MatDialog,) {
    this.sharedService.claerSearchValue.subscribe((data) => {
      if (data != null) {
        this.inputName.nativeElement.value = null;
        this.searchData = null;
      }
    })
  }

  ngOnInit(): void {

  }

  search() {

    if (this.searchData == undefined || this.searchData == '') {
      return;
    }
    this.executeListing(this.searchData)
  }


  onKeySearch(event: any) {
    this.searchData = event.target.value;
    clearTimeout(this.timeout);
    var $this = this;
    this.timeout = setTimeout(function () {
      if (event.keyCode == 13) {
        this.searchData = event.target.value;
        if (this.searchData != '') {
          $this.executeListing(this.searchData);
        }
      }
    }, 1000);

    this.timeout = setTimeout(function () {
      if (event.keyCode != 13) {
        this.searchData = event.target.value;
        $this.executeListing(this.searchData);
      }
    }, 1000);

  }


  executeListing(value: string) {
    if (value == '') {
      let searchValue = {
        search: value,
        flag: false
      };
      this.sharedService.searchData(searchValue);
    } else {

      let searchValue = {
        search: value,
        flag: true
      };
      this.sharedService.searchData(searchValue);

    }
  }

  clearCredentials(): void {
    this.openClose=!this.openClose
      let type = 'logout'
      const dialogRef = this.dialog.open(DeleteVideoComponent, { panelClass: 'delete-video', data: { type } });
      dialogRef.afterClosed().subscribe(result => {

      });
    } 
  


}

import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './external/login/login.component';
import { OtpComponent } from './external/otp/otp.component';
import { NgOtpInputModule } from  'ng-otp-input';
import { ToastrModule } from 'ngx-toastr';
import { ReactiveFormsModule } from '@angular/forms';
import { ExternalAuthguardService } from './services/externalAuthguard.service';
import { HttpService } from './services/http.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorService } from './services/interceptor.service';
import { VideoPreviewComponent } from './model/video-preview/video-preview.component';
import { MatDialogModule } from '@angular/material/dialog';
import { SharedService } from './services/shared.service';
import { DeleteVideoComponent } from './model/delete-video/delete-video.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ApproveVideoComponent } from './model/approve-video/approve-video.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    OtpComponent,
    DeleteVideoComponent,
    ApproveVideoComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgOtpInputModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    MatDialogModule,
    BsDropdownModule.forRoot() 
    
    
 
    
  ],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    },
    HttpService,
    ExternalAuthguardService,
    SharedService

  ],
  entryComponents: [
  VideoPreviewComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ],
  
})
export class AppModule { }

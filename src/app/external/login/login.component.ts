import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiUrl } from 'src/app/services/apiurl';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  button = 'LOGIN';
  isLoading = false;

  constructor(private formBuilder: FormBuilder, public http: HttpService, private _router: Router, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.login()
  }


  login() {

    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]]
    });

  }
  get f() { return this.loginForm.controls; }


  
  keyDownLogin(event) {
    if (event.keyCode == 13) {
      this.onSubmit();
    }
  }

  onSubmit() {

    this.submitted = true;

     if (this.toastr.currentlyActive) {
        return
      }

    if (this.loginForm.value.email=="") {
          this.toastr.error('Email is required', 'Invalid', {
        timeOut: 3000
      });
      return;
    }
  
    if (this.loginForm.invalid) {
      this.toastr.error('Please enter a valid email address', 'Invalid', {
        timeOut: 3000
      });
      return;
    }

    this.isLoading = true;
    this.button = 'PROCESSING';

    let payload = {
      email: this.loginForm.value.email,
      type: 'EMAIL_VERIFICATION'
    }

    this.http.postData(ApiUrl.login, payload, false)
      .subscribe(res => {

        if (res.statusCode == 200) {
          this.toastr.success('Otp sent successfully', 'success', {
            timeOut: 2000
          });
          this.isLoading = false;
          this.button = 'LOGIN';
          this._router.navigate(['/otp'], { queryParams: { email: this.loginForm.value.email } });

        } else {

          this.toastr.error('otp not send', 'Error', {
            timeOut: 3000
          });

        }
      },
        () => {
          this.isLoading = false;
          this.button = 'LOGIN';
        });
  }


}

import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { ApiUrl } from 'src/app/services/apiurl';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.scss']
})
export class OtpComponent implements OnInit {
  otp: string;
  showOtpComponent = true;
  email:any
  button = 'DONE';
  isLoading = false;

  constructor(private _router: Router,
    private _route: ActivatedRoute,public http: HttpService,private toastr: ToastrService) { }

  ngOnInit(): void {
    this._route.queryParams.subscribe(params => {
      this.email = params['email'];
    });
  }
  @ViewChild('ngOtpInput', { static: false}) ngOtpInput: any;
  config = {
    allowNumbersOnly: false,
    length: 6,
    isPasswordInput: false,
    disableAutoFocus: false,
    placeholder: '-',
    inputStyles: {
      'width': '50px',
      'height': '50px'
    }
  };

  onOtpChange(otp) {
    this.otp = otp;
  }

  save(){

    this.isLoading = true;
    this.button = 'Processing';

    let payload={
      email:this.email,
      type:'EMAIL_VERIFICATION',
      code:this.otp

    }

    this.http.postData(ApiUrl.verifyotp, payload,false)
    .subscribe(res => {
      if (res.statusCode == 200) {
      localStorage.setItem('accessToken', res.data.accessToken);
      localStorage.setItem('loginData', JSON.stringify(res.data));
      this.isLoading = false;
      this.button = 'DONE';
      this.toastr.success('Otp verify successfully', 'success', {
        timeOut: 2000
      });
      window.location.href = "dashboard";

      }else{
        this.isLoading = false;
        this.button = 'DONE';
        this.toastr.error('OTP not send', 'Invalid', {
          timeOut: 3000
        });
        
      }
   
    
    },
      () => {

        this.isLoading = false;
        this.button = 'DONE';
       
      });

  }


  keyDownLogin(event) {
    if (event.keyCode == 13) {
      this.save();
    }
  }

  resendOtp(){

    let payload={
      email:this.email,
      type:'EMAIL_VERIFICATION',
    }

    if (this.toastr.currentlyActive) {
      return
    }

    this.http.postData(ApiUrl.resendotp, payload,false)
    .subscribe(res => {

      if (res.statusCode == 200) {
        this.toastr.success('OTP resend successfully', 'success', {
          timeOut: 2000
        });

      }else{
        if (this.toastr.currentlyActive) {
          return
        }  
        this.toastr.error('OTP not send', 'Error', {
          timeOut: 3000
        });
      }
    
    },
      () => {
       
      });


  }
}
